## Tutorial
https://www.digitalocean.com/community/tutorials/containerizing-a-node-js-application-for-development-with-docker-compose

Project code for tutorial on integrating MongoDB into Node.js application using Mongoose: https://www.digitalocean.com/community/tutorials/how-to-integrate-mongodb-with-your-node-application

## Puertos en USO
- https://www.cyberciti.biz/faq/unix-linux-check-if-port-is-in-use-command/
- sudo lsof -i -P -n | grep LISTEN 

# Tutorial Mongo
https://www.tutorialspoint.com/mongodb/mongodb_create_database.htm
## Mongo Import
https://docs.mongodb.com/manual/reference/program/mongoimport/


# Pasar datos de afuera
https://stackoverflow.com/questions/44310917/mongodb-docker-creating-initial-users-and-setting-up-initial-structures

## Ingresar a mongo
```yml
mongo -u root -p root
```
## Ver databases
```yml
show dbs
```
## Seleccionar BD
```yml
use sa
```
## Ver colecciones
```yml
show collections
```
## Ver elementos
```yml
db.sharks.find().pretty()
```